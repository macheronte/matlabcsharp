﻿using System;
using System.Runtime.InteropServices;

namespace MatlabCSharp
{
    class Program
    {
        [DllImport(@"./h_rand.dll",
           CallingConvention = CallingConvention.Cdecl)]
        public static extern void h_rand(uint row, uint col, ref emxArray_real_T output);
        static void Main(string[] args)
        {
            int rowNum = 5;
            int colNum = 5;

            double[] matrixVector = new double[rowNum * colNum];

            using (emxArray_real_T_Wrapper wb = new emxArray_real_T_Wrapper(matrixVector))
            {
                h_rand(5, 5, ref wb.Value);
                matrixVector = wb.Data;
            }

            for (int i = 0; i < rowNum; i++)
            {
                for (int j = 0; j < colNum; j++)
                {
                    Console.Write(string.Format("{0} ", matrixVector[i + j * rowNum]));
                }
                Console.WriteLine();
            }

        }

        public struct emxArray_real_T
        {
            public IntPtr data;
            public IntPtr size;
            public int allocatedSize;
            public int numDimensions;
            public bool canFreeData;
        }

        public class emxArray_real_T_Wrapper : IDisposable
        {
            private emxArray_real_T value;
            private GCHandle dataHandle;
            private GCHandle sizeHandle;

            public ref emxArray_real_T Value
            {
                get { return ref value; }
            }

            public double[] Data
            {
                get
                {
                    double[] data = new double[value.allocatedSize];
                    Marshal.Copy(value.data, data, 0, value.allocatedSize);
                    return data;
                }
            }

            public emxArray_real_T_Wrapper(double aData)
            {
                double[] data = { aData };
                dataHandle = GCHandle.Alloc(data, GCHandleType.Pinned);
                value.data = dataHandle.AddrOfPinnedObject();
                sizeHandle = GCHandle.Alloc(new int[] { 1, data.Length }, GCHandleType.Pinned);
                value.size = sizeHandle.AddrOfPinnedObject();
                value.allocatedSize = data.Length;
                value.numDimensions = 1;
                value.canFreeData = false;
            }

            public emxArray_real_T_Wrapper(double[] data)
            {
                dataHandle = GCHandle.Alloc(data, GCHandleType.Pinned);
                value.data = dataHandle.AddrOfPinnedObject();
                sizeHandle = GCHandle.Alloc(new int[] { 1, data.Length }, GCHandleType.Pinned);
                value.size = sizeHandle.AddrOfPinnedObject();
                value.allocatedSize = data.Length;
                value.numDimensions = 1;
                value.canFreeData = false;
            }

            public emxArray_real_T_Wrapper(double[,] data)
            {
                int nRow = data.GetLength(0);
                int nCol = data.GetLength(1);

                double[] flattenedData = new double[nCol * nRow];
                int index = 0;
                for (int col = 0; col < nCol; col++)
                {
                    for (int row = 0; row < nRow; row++)
                    {
                        flattenedData[index] = data[row, col];
                        index++;
                    }
                }

                dataHandle = GCHandle.Alloc(flattenedData, GCHandleType.Pinned);
                value.data = dataHandle.AddrOfPinnedObject();
                sizeHandle = GCHandle.Alloc(new int[] { nRow, nCol }, GCHandleType.Pinned);
                value.size = sizeHandle.AddrOfPinnedObject();
                value.allocatedSize = flattenedData.Length;
                value.numDimensions = 2;
                value.canFreeData = false;
            }

            public void Dispose()
            {
                dataHandle.Free();
                sizeHandle.Free();
                GC.SuppressFinalize(this);
            }

            ~emxArray_real_T_Wrapper()
            {
                Dispose();
            }
        }
    }
}
